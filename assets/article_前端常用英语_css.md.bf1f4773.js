import{_ as s,o as n,c as a,Q as l}from"./chunks/framework.f25890ca.js";const u=JSON.parse('{"title":"CSS (常用单词)","description":"","frontmatter":{},"headers":[],"relativePath":"article/前端常用英语/css.md","filePath":"article/前端常用英语/css.md","lastUpdated":1690446826000}'),p={name:"article/前端常用英语/css.md"},e=l(`<h1 id="css-常用单词" tabindex="-1">CSS (常用单词) <a class="header-anchor" href="#css-常用单词" aria-label="Permalink to &quot;CSS (常用单词)&quot;">​</a></h1><div class="language-css vp-adaptive-theme line-numbers-mode"><button title="Copy Code" class="copy"></button><span class="lang">css</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#E1E4E8;">color</span></span>
<span class="line"><span style="color:#E1E4E8;">定义颜色</span></span>
<span class="line"><span style="color:#E1E4E8;">opacity</span></span>
<span class="line"><span style="color:#E1E4E8;">检索或设置对象的不透明度</span></span>
<span class="line"><span style="color:#85E89D;">font-size</span></span>
<span class="line"><span style="color:#E1E4E8;">定义字体大小</span></span>
<span class="line"><span style="color:#85E89D;">font-style</span></span>
<span class="line"><span style="color:#E1E4E8;">定义字体体风格例如斜体</span></span>
<span class="line"><span style="color:#85E89D;">font-weight</span></span>
<span class="line"><span style="color:#E1E4E8;">定义字体粗细</span></span>
<span class="line"><span style="color:#85E89D;">font-family</span></span>
<span class="line"><span style="color:#E1E4E8;">嵌入字体</span></span>
<span class="line"><span style="color:#85E89D;">text-indent</span></span>
<span class="line"><span style="color:#E1E4E8;">首行缩进</span></span>
<span class="line"><span style="color:#85E89D;">text-align</span></span>
<span class="line"><span style="color:#E1E4E8;">水平对其方式</span></span>
<span class="line"><span style="color:#85E89D;">text-overflow</span></span>
<span class="line"><span style="color:#E1E4E8;">文本溢出</span></span>
<span class="line"><span style="color:#85E89D;">text-shadow</span></span>
<span class="line"><span style="color:#E1E4E8;">文本阴影</span></span>
<span class="line"><span style="color:#85E89D;">white-space</span></span>
<span class="line"><span style="color:#E1E4E8;">处理换行和空格</span></span>
<span class="line"><span style="color:#85E89D;">line-height</span></span>
<span class="line"><span style="color:#E1E4E8;">行高</span></span>
<span class="line"><span style="color:#E1E4E8;">background</span></span>
<span class="line"><span style="color:#E1E4E8;">定义背景</span></span>
<span class="line"><span style="color:#85E89D;">background-color</span></span>
<span class="line"><span style="color:#E1E4E8;">指定背景颜色</span></span>
<span class="line"><span style="color:#85E89D;">background-image</span></span>
<span class="line"><span style="color:#E1E4E8;">背景图片</span></span>
<span class="line"><span style="color:#85E89D;">background-repeat</span></span>
<span class="line"><span style="color:#E1E4E8;">背景平铺排列</span></span>
<span class="line"><span style="color:#85E89D;">background-position</span></span>
<span class="line"><span style="color:#E1E4E8;">背景定位</span></span>
<span class="line"><span style="color:#85E89D;">background-attachment</span></span>
<span class="line"><span style="color:#E1E4E8;">背景是否固定</span></span>
<span class="line"><span style="color:#E1E4E8;">border</span></span>
<span class="line"><span style="color:#E1E4E8;">定义边框</span></span>
<span class="line"><span style="color:#85E89D;">border-color</span></span>
<span class="line"><span style="color:#E1E4E8;">边框颜色</span></span>
<span class="line"><span style="color:#85E89D;">border-radius</span></span>
<span class="line"><span style="color:#E1E4E8;">边框圆角</span></span>
<span class="line"><span style="color:#85E89D;">border-style</span></span>
<span class="line"><span style="color:#E1E4E8;">边框样式</span></span>
<span class="line"><span style="color:#85E89D;">border-image</span></span>
<span class="line"><span style="color:#E1E4E8;">边框图片</span></span>
<span class="line"><span style="color:#85E89D;">box-shadow</span></span>
<span class="line"><span style="color:#E1E4E8;">阴影</span></span>
<span class="line"><span style="color:#E1E4E8;">padding</span></span>
<span class="line"><span style="color:#E1E4E8;">内边距</span></span>
<span class="line"><span style="color:#E1E4E8;">margin</span></span>
<span class="line"><span style="color:#E1E4E8;">外边距</span></span>
<span class="line"><span style="color:#E1E4E8;">width</span></span>
<span class="line"><span style="color:#E1E4E8;">宽度</span></span>
<span class="line"><span style="color:#E1E4E8;">height</span></span>
<span class="line"><span style="color:#E1E4E8;">高度</span></span>
<span class="line"><span style="color:#85E89D;">max-width</span><span style="color:#E1E4E8;"> </span><span style="color:#85E89D;">min-width</span></span>
<span class="line"><span style="color:#E1E4E8;">最大宽度 最小宽度</span></span>
<span class="line"><span style="color:#85E89D;">max-height</span><span style="color:#E1E4E8;"> </span><span style="color:#85E89D;">min-height</span></span>
<span class="line"><span style="color:#E1E4E8;">最大高度 最小高度</span></span>
<span class="line"><span style="color:#E1E4E8;">display flex</span></span>
<span class="line"><span style="color:#E1E4E8;">弹性盒</span></span>
<span class="line"><span style="color:#85E89D;">flex-direction</span></span>
<span class="line"><span style="color:#E1E4E8;">调整主轴XY</span></span>
<span class="line"><span style="color:#85E89D;">flex-wrap</span></span>
<span class="line"><span style="color:#E1E4E8;">换行</span></span>
<span class="line"><span style="color:#85E89D;">justify-content</span></span>
<span class="line"><span style="color:#E1E4E8;">水平方位调整</span></span>
<span class="line"><span style="color:#85E89D;">align-items</span></span>
<span class="line"><span style="color:#E1E4E8;">垂直方位调整</span></span>
<span class="line"><span style="color:#85E89D;">flex-grow</span></span>
<span class="line"><span style="color:#E1E4E8;">调整比例</span></span>
<span class="line"><span style="color:#85E89D;">flex-shrink</span></span>
<span class="line"><span style="color:#E1E4E8;">flex 元素的收缩规则</span></span>
<span class="line"><span style="color:#85E89D;">flex-basis</span></span>
<span class="line"><span style="color:#E1E4E8;">指定 flex 元素在主轴方向上的初始大小</span></span>
<span class="line"><span style="color:#E1E4E8;">flex</span></span>
<span class="line"><span style="color:#E1E4E8;">三个属性合体flex-grow </span><span style="color:#85E89D;">flex-shrink</span><span style="color:#E1E4E8;"> </span><span style="color:#85E89D;">flex-basis</span></span>
<span class="line"><span style="color:#E1E4E8;">position</span></span>
<span class="line"><span style="color:#E1E4E8;">固定定位 相对定位 绝对定位 粘性定位</span></span>
<span class="line"><span style="color:#E1E4E8;">left  top bottom right</span></span>
<span class="line"><span style="color:#E1E4E8;">四个方位</span></span>
<span class="line"><span style="color:#85E89D;">z-index</span></span>
<span class="line"><span style="color:#E1E4E8;">定位层级</span></span>
<span class="line"><span style="color:#E1E4E8;">transform</span></span>
<span class="line"><span style="color:#E1E4E8;">旋转，缩放，倾斜或平移给定元素</span></span>
<span class="line"><span style="color:#E1E4E8;">transition</span></span>
<span class="line"><span style="color:#E1E4E8;">动画过度</span></span>
<span class="line"><span style="color:#E1E4E8;">animation</span></span>
<span class="line"><span style="color:#E1E4E8;">动画</span></span>
<span class="line"><span style="color:#E1E4E8;">linear-gradient()</span></span>
<span class="line"><span style="color:#E1E4E8;">颜色渐变</span></span>
<span class="line"><span style="color:#85E89D;">filter</span></span>
<span class="line"><span style="color:#E1E4E8;">背景模糊 变色等</span></span>
<span class="line"><span style="color:#E1E4E8;">calc()</span></span>
<span class="line"><span style="color:#E1E4E8;">计算函数</span></span>
<span class="line"><span style="color:#E1E4E8;">overflow</span></span>
<span class="line"><span style="color:#E1E4E8;">内容溢出操作</span></span>
<span class="line"><span style="color:#E1E4E8;">float</span></span>
<span class="line"><span style="color:#E1E4E8;">浮动</span></span>
<span class="line"><span style="color:#E1E4E8;">clear</span></span>
<span class="line"><span style="color:#E1E4E8;">清除浮动</span></span>
<span class="line"><span style="color:#E1E4E8;">长度 px rem </span><span style="color:#85E89D;">em</span><span style="color:#E1E4E8;"> vh vw cm mm pt pc ch ex</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#24292E;">color</span></span>
<span class="line"><span style="color:#24292E;">定义颜色</span></span>
<span class="line"><span style="color:#24292E;">opacity</span></span>
<span class="line"><span style="color:#24292E;">检索或设置对象的不透明度</span></span>
<span class="line"><span style="color:#22863A;">font-size</span></span>
<span class="line"><span style="color:#24292E;">定义字体大小</span></span>
<span class="line"><span style="color:#22863A;">font-style</span></span>
<span class="line"><span style="color:#24292E;">定义字体体风格例如斜体</span></span>
<span class="line"><span style="color:#22863A;">font-weight</span></span>
<span class="line"><span style="color:#24292E;">定义字体粗细</span></span>
<span class="line"><span style="color:#22863A;">font-family</span></span>
<span class="line"><span style="color:#24292E;">嵌入字体</span></span>
<span class="line"><span style="color:#22863A;">text-indent</span></span>
<span class="line"><span style="color:#24292E;">首行缩进</span></span>
<span class="line"><span style="color:#22863A;">text-align</span></span>
<span class="line"><span style="color:#24292E;">水平对其方式</span></span>
<span class="line"><span style="color:#22863A;">text-overflow</span></span>
<span class="line"><span style="color:#24292E;">文本溢出</span></span>
<span class="line"><span style="color:#22863A;">text-shadow</span></span>
<span class="line"><span style="color:#24292E;">文本阴影</span></span>
<span class="line"><span style="color:#22863A;">white-space</span></span>
<span class="line"><span style="color:#24292E;">处理换行和空格</span></span>
<span class="line"><span style="color:#22863A;">line-height</span></span>
<span class="line"><span style="color:#24292E;">行高</span></span>
<span class="line"><span style="color:#24292E;">background</span></span>
<span class="line"><span style="color:#24292E;">定义背景</span></span>
<span class="line"><span style="color:#22863A;">background-color</span></span>
<span class="line"><span style="color:#24292E;">指定背景颜色</span></span>
<span class="line"><span style="color:#22863A;">background-image</span></span>
<span class="line"><span style="color:#24292E;">背景图片</span></span>
<span class="line"><span style="color:#22863A;">background-repeat</span></span>
<span class="line"><span style="color:#24292E;">背景平铺排列</span></span>
<span class="line"><span style="color:#22863A;">background-position</span></span>
<span class="line"><span style="color:#24292E;">背景定位</span></span>
<span class="line"><span style="color:#22863A;">background-attachment</span></span>
<span class="line"><span style="color:#24292E;">背景是否固定</span></span>
<span class="line"><span style="color:#24292E;">border</span></span>
<span class="line"><span style="color:#24292E;">定义边框</span></span>
<span class="line"><span style="color:#22863A;">border-color</span></span>
<span class="line"><span style="color:#24292E;">边框颜色</span></span>
<span class="line"><span style="color:#22863A;">border-radius</span></span>
<span class="line"><span style="color:#24292E;">边框圆角</span></span>
<span class="line"><span style="color:#22863A;">border-style</span></span>
<span class="line"><span style="color:#24292E;">边框样式</span></span>
<span class="line"><span style="color:#22863A;">border-image</span></span>
<span class="line"><span style="color:#24292E;">边框图片</span></span>
<span class="line"><span style="color:#22863A;">box-shadow</span></span>
<span class="line"><span style="color:#24292E;">阴影</span></span>
<span class="line"><span style="color:#24292E;">padding</span></span>
<span class="line"><span style="color:#24292E;">内边距</span></span>
<span class="line"><span style="color:#24292E;">margin</span></span>
<span class="line"><span style="color:#24292E;">外边距</span></span>
<span class="line"><span style="color:#24292E;">width</span></span>
<span class="line"><span style="color:#24292E;">宽度</span></span>
<span class="line"><span style="color:#24292E;">height</span></span>
<span class="line"><span style="color:#24292E;">高度</span></span>
<span class="line"><span style="color:#22863A;">max-width</span><span style="color:#24292E;"> </span><span style="color:#22863A;">min-width</span></span>
<span class="line"><span style="color:#24292E;">最大宽度 最小宽度</span></span>
<span class="line"><span style="color:#22863A;">max-height</span><span style="color:#24292E;"> </span><span style="color:#22863A;">min-height</span></span>
<span class="line"><span style="color:#24292E;">最大高度 最小高度</span></span>
<span class="line"><span style="color:#24292E;">display flex</span></span>
<span class="line"><span style="color:#24292E;">弹性盒</span></span>
<span class="line"><span style="color:#22863A;">flex-direction</span></span>
<span class="line"><span style="color:#24292E;">调整主轴XY</span></span>
<span class="line"><span style="color:#22863A;">flex-wrap</span></span>
<span class="line"><span style="color:#24292E;">换行</span></span>
<span class="line"><span style="color:#22863A;">justify-content</span></span>
<span class="line"><span style="color:#24292E;">水平方位调整</span></span>
<span class="line"><span style="color:#22863A;">align-items</span></span>
<span class="line"><span style="color:#24292E;">垂直方位调整</span></span>
<span class="line"><span style="color:#22863A;">flex-grow</span></span>
<span class="line"><span style="color:#24292E;">调整比例</span></span>
<span class="line"><span style="color:#22863A;">flex-shrink</span></span>
<span class="line"><span style="color:#24292E;">flex 元素的收缩规则</span></span>
<span class="line"><span style="color:#22863A;">flex-basis</span></span>
<span class="line"><span style="color:#24292E;">指定 flex 元素在主轴方向上的初始大小</span></span>
<span class="line"><span style="color:#24292E;">flex</span></span>
<span class="line"><span style="color:#24292E;">三个属性合体flex-grow </span><span style="color:#22863A;">flex-shrink</span><span style="color:#24292E;"> </span><span style="color:#22863A;">flex-basis</span></span>
<span class="line"><span style="color:#24292E;">position</span></span>
<span class="line"><span style="color:#24292E;">固定定位 相对定位 绝对定位 粘性定位</span></span>
<span class="line"><span style="color:#24292E;">left  top bottom right</span></span>
<span class="line"><span style="color:#24292E;">四个方位</span></span>
<span class="line"><span style="color:#22863A;">z-index</span></span>
<span class="line"><span style="color:#24292E;">定位层级</span></span>
<span class="line"><span style="color:#24292E;">transform</span></span>
<span class="line"><span style="color:#24292E;">旋转，缩放，倾斜或平移给定元素</span></span>
<span class="line"><span style="color:#24292E;">transition</span></span>
<span class="line"><span style="color:#24292E;">动画过度</span></span>
<span class="line"><span style="color:#24292E;">animation</span></span>
<span class="line"><span style="color:#24292E;">动画</span></span>
<span class="line"><span style="color:#24292E;">linear-gradient()</span></span>
<span class="line"><span style="color:#24292E;">颜色渐变</span></span>
<span class="line"><span style="color:#22863A;">filter</span></span>
<span class="line"><span style="color:#24292E;">背景模糊 变色等</span></span>
<span class="line"><span style="color:#24292E;">calc()</span></span>
<span class="line"><span style="color:#24292E;">计算函数</span></span>
<span class="line"><span style="color:#24292E;">overflow</span></span>
<span class="line"><span style="color:#24292E;">内容溢出操作</span></span>
<span class="line"><span style="color:#24292E;">float</span></span>
<span class="line"><span style="color:#24292E;">浮动</span></span>
<span class="line"><span style="color:#24292E;">clear</span></span>
<span class="line"><span style="color:#24292E;">清除浮动</span></span>
<span class="line"><span style="color:#24292E;">长度 px rem </span><span style="color:#22863A;">em</span><span style="color:#24292E;"> vh vw cm mm pt pc ch ex</span></span></code></pre><div class="line-numbers-wrapper" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br></div></div>`,2),c=[e];function o(r,i,t,E,b,y){return n(),a("div",null,c)}const d=s(p,[["render",o]]);export{u as __pageData,d as default};
