import{_ as s,o as n,c as a,Q as l}from"./chunks/framework.f25890ca.js";const d=JSON.parse('{"title":"JavaScript (常用单词)","description":"","frontmatter":{},"headers":[],"relativePath":"article/前端常用英语/js.md","filePath":"article/前端常用英语/js.md","lastUpdated":1690446826000}'),p={name:"article/前端常用英语/js.md"},e=l(`<h1 id="javascript-常用单词" tabindex="-1">JavaScript (常用单词) <a class="header-anchor" href="#javascript-常用单词" aria-label="Permalink to &quot;JavaScript (常用单词)&quot;">​</a></h1><div class="language-js vp-adaptive-theme line-numbers-mode"><button title="Copy Code" class="copy"></button><span class="lang">js</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#F97583;">var</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">let</span><span style="color:#E1E4E8;"> const</span></span>
<span class="line"><span style="color:#E1E4E8;">声明变量</span></span>
<span class="line"><span style="color:#F97583;">if</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">else</span></span>
<span class="line"><span style="color:#E1E4E8;">条件语句</span></span>
<span class="line"><span style="color:#F97583;">switch</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">case</span></span>
<span class="line"><span style="color:#E1E4E8;">条件语句</span></span>
<span class="line"><span style="color:#E1E4E8;">alert confirm prompt</span></span>
<span class="line"><span style="color:#E1E4E8;">弹框</span></span>
<span class="line"><span style="color:#F97583;">function</span></span>
<span class="line"><span style="color:#B392F0;">函数</span></span>
<span class="line"><span style="color:#E1E4E8;">for</span></span>
<span class="line"><span style="color:#E1E4E8;">循环</span></span>
<span class="line"><span style="color:#F97583;">while</span></span>
<span class="line"><span style="color:#E1E4E8;">循环</span></span>
<span class="line"><span style="color:#F97583;">break</span></span>
<span class="line"><span style="color:#E1E4E8;">终止循环</span></span>
<span class="line"><span style="color:#F97583;">continue</span></span>
<span class="line"><span style="color:#E1E4E8;">跳过循环</span></span>
<span class="line"><span style="color:#E1E4E8;">onLoad onUnload onFocus onBlur onChange onSubmit onClick onMouseOver onMouseOut</span></span>
<span class="line"><span style="color:#E1E4E8;">事件太多了随便列举几个</span></span>
<span class="line"><span style="color:#F97583;">try</span><span style="color:#E1E4E8;"> </span><span style="color:#F97583;">catch</span></span>
<span class="line"><span style="color:#E1E4E8;">异常捕获</span></span>
<span class="line"><span style="color:#F97583;">throw</span></span>
<span class="line"><span style="color:#E1E4E8;">抛异常</span></span>
<span class="line"><span style="color:#E1E4E8;">String Number Boolean Null Object Array Symbol BigInt </span><span style="color:#79B8FF;">undefined</span></span>
<span class="line"><span style="color:#E1E4E8;">基本类型</span></span>
<span class="line"><span style="color:#79B8FF;">Promise</span><span style="color:#E1E4E8;"> then </span><span style="color:#F97583;">catch</span><span style="color:#E1E4E8;"> reject resolve all any race allSettled</span></span>
<span class="line"><span style="color:#79B8FF;">DOM</span></span>
<span class="line"><span style="color:#E1E4E8;">window location screen history Navigator</span></span>
<span class="line"><span style="color:#79B8FF;">BOM</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.nodeName   </span><span style="color:#6A737D;">//返回节点名称</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.nodeType   </span><span style="color:#6A737D;">//返回节点类型的常数值</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.nodeValue  </span><span style="color:#6A737D;">//返回Text或Comment节点的文本值</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.textContent  </span><span style="color:#6A737D;">//返回当前节点和它的所有后代节点的文本内容，可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.baseURI    </span><span style="color:#6A737D;">//返回当前网页的绝对路径</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.ownerDocument  </span><span style="color:#6A737D;">//返回当前节点所在的顶层文档对象，即document</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.nextSibling  </span><span style="color:#6A737D;">//返回紧跟在当前节点后面的第一个兄弟节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.previousSibling  </span><span style="color:#6A737D;">//返回当前节点前面的、距离最近的一个兄弟节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.parentNode   </span><span style="color:#6A737D;">//返回当前节点的父节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.parentElement  </span><span style="color:#6A737D;">//返回当前节点的父Element节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.childNodes   </span><span style="color:#6A737D;">//返回当前节点的所有子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.firstChild  </span><span style="color:#6A737D;">//返回当前节点的第一个子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.lastChild   </span><span style="color:#6A737D;">//返回当前节点的最后一个子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.isconnecoted   </span><span style="color:#6A737D;">//返回一个布尔值，表示当前节点是否在文档之中</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">appendChild</span><span style="color:#E1E4E8;">(node)   </span><span style="color:#6A737D;">//向节点添加最后一个子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">hasChildNodes</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//返回布尔值，表示当前节点是否有子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">cloneNode</span><span style="color:#E1E4E8;">(</span><span style="color:#79B8FF;">true</span><span style="color:#E1E4E8;">);  </span><span style="color:#6A737D;">// 默认为false(克隆节点), true(克隆节点及其属性，以及后代)</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">insertBefore</span><span style="color:#E1E4E8;">(newNode,oldNode)  </span><span style="color:#6A737D;">// 在指定子节点之前插入新的子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">removeChild</span><span style="color:#E1E4E8;">(node)   </span><span style="color:#6A737D;">//删除节点，在要删除节点的父节点上操作</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">replaceChild</span><span style="color:#E1E4E8;">(newChild,oldChild)  </span><span style="color:#6A737D;">//替换节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">contains</span><span style="color:#E1E4E8;">(node)  </span><span style="color:#6A737D;">//返回一个布尔值，表示参数节点是否为当前节点的后代节点。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">compareDocumentPosition</span><span style="color:#E1E4E8;">(node)   </span><span style="color:#6A737D;">//返回一个7个比特位的二进制值，表示参数节点和当前节点的关系</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">isEqualNode</span><span style="color:#E1E4E8;">(node)  </span><span style="color:#6A737D;">//返回布尔值，用于检查两个节点是否相等。所谓相等的节点，指的是两个节点的类型相同、属性相同、子节点相同。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">isSameNode</span><span style="color:#E1E4E8;">(node)  </span><span style="color:#6A737D;">//返回一个布尔值，表示两个节点是否为同一个节点。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">normalize</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//用于清理当前节点内部的所有Text节点。它会去除空的文本节点，并且将毗邻的文本节点合并成一个。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">getRootNode</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//返回当前节点所在文档的根节点document，与ownerDocument属性的作用相同。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#79B8FF;">length</span><span style="color:#E1E4E8;">   </span><span style="color:#6A737D;">//返回 NodeList 实例包含的节点数量</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">forEach</span><span style="color:#E1E4E8;">(fn，</span><span style="color:#79B8FF;">this</span><span style="color:#E1E4E8;">)   </span><span style="color:#6A737D;">//用于遍历 NodeList 的所有成员</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">item</span><span style="color:#E1E4E8;">(index) </span><span style="color:#6A737D;">// 接受一个整数值作为参数，表示成员的位置，返回该位置上的成员</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">keys</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//返回键名的遍历器</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">values</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//返回键值的遍历器</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">entries</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//返回的遍历器同时包含键名和键值的信息</span></span>
<span class="line"><span style="color:#6A737D;">//parentNode接口</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.children  </span><span style="color:#6A737D;">//返回指定节点的所有Element子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.firstElementChild  </span><span style="color:#6A737D;">//返回当前节点的第一个Element子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.lastElementChild   </span><span style="color:#6A737D;">//返回当前节点的最后一个Element子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.childElementCount  </span><span style="color:#6A737D;">//返回当前节点所有Element子节点的数目</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">append</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//为当前节点追加一个或多个子节点，位置是最后一个元素子节点的后面。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">prepend</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//为当前节点追加一个或多个子节点，位置是第一个元素子节点的前面。</span></span>
<span class="line"><span style="color:#6A737D;">//ChildNode接口</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">remove</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//用于从父节点移除当前节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">before</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//用于在当前节点的前面，插入一个或多个同级节点。两者拥有相同的父节点。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">after</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//用于在当前节点的后面，插入一个或多个同级节点，两者拥有相同的父节点。</span></span>
<span class="line"><span style="color:#E1E4E8;">Node.</span><span style="color:#B392F0;">replaceWith</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//使用参数节点，替换当前节点</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">document</span></span>
<span class="line"><span style="color:#6A737D;">//快捷方式属性</span></span>
<span class="line"><span style="color:#E1E4E8;">document.defaultView </span><span style="color:#6A737D;">//返回document对象所属的window对象</span></span>
<span class="line"><span style="color:#E1E4E8;">document.doctype   </span><span style="color:#6A737D;">//返回文档类型节点</span></span>
<span class="line"><span style="color:#E1E4E8;">document.documentElement  </span><span style="color:#6A737D;">//返回当前文档的根节点</span></span>
<span class="line"><span style="color:#E1E4E8;">document.body   </span><span style="color:#6A737D;">//返回当前文档的&lt;body&gt;节点</span></span>
<span class="line"><span style="color:#E1E4E8;">document.head   </span><span style="color:#6A737D;">//返回当前文档的&lt;head&gt;节点</span></span>
<span class="line"><span style="color:#E1E4E8;">document.activeElement  </span><span style="color:#6A737D;">//返回当前文档中获得焦点的那个元素</span></span>
<span class="line"><span style="color:#E1E4E8;">document.fullscreenElement  </span><span style="color:#6A737D;">//返回当前以全屏状态展示的 DOM 元素</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//节点集合属性</span></span>
<span class="line"><span style="color:#E1E4E8;">document.links  </span><span style="color:#6A737D;">//返回当前文档的所有a元素</span></span>
<span class="line"><span style="color:#E1E4E8;">document.forms  </span><span style="color:#6A737D;">//返回页面中所有表单元素</span></span>
<span class="line"><span style="color:#E1E4E8;">document.images  </span><span style="color:#6A737D;">//返回页面中所有图片元素</span></span>
<span class="line"><span style="color:#E1E4E8;">document.embeds  </span><span style="color:#6A737D;">//返回网页中所有嵌入对象</span></span>
<span class="line"><span style="color:#E1E4E8;">document.scripts  </span><span style="color:#6A737D;">//返回当前文档的所有脚本</span></span>
<span class="line"><span style="color:#E1E4E8;">document.styleSheets  </span><span style="color:#6A737D;">//返回当前网页的所有样式表</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//文档静态信息属性</span></span>
<span class="line"><span style="color:#E1E4E8;">document.documentURI  </span><span style="color:#6A737D;">//表示当前文档的网址</span></span>
<span class="line"><span style="color:#E1E4E8;">document.</span><span style="color:#79B8FF;">URL</span><span style="color:#E1E4E8;">  </span><span style="color:#6A737D;">//返回当前文档的网址</span></span>
<span class="line"><span style="color:#E1E4E8;">document.domain  </span><span style="color:#6A737D;">//返回当前文档的域名</span></span>
<span class="line"><span style="color:#E1E4E8;">document.lastModified  </span><span style="color:#6A737D;">//返回当前文档最后修改的时间</span></span>
<span class="line"><span style="color:#E1E4E8;">document.location  </span><span style="color:#6A737D;">//返回location对象，提供当前文档的URL信息</span></span>
<span class="line"><span style="color:#E1E4E8;">document.referrer  </span><span style="color:#6A737D;">//返回当前文档的访问来源</span></span>
<span class="line"><span style="color:#E1E4E8;">document.title    </span><span style="color:#6A737D;">//返回当前文档的标题</span></span>
<span class="line"><span style="color:#E1E4E8;">document.compatMode  </span><span style="color:#6A737D;">//返回浏览器处理文档的模式</span></span>
<span class="line"><span style="color:#E1E4E8;">document.characterSet  </span><span style="color:#6A737D;">//返回渲染当前文档的字符集，比如UTF-8、ISO-8859-1</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//文档状态属性</span></span>
<span class="line"><span style="color:#E1E4E8;">document.readyState  </span><span style="color:#6A737D;">//返回当前文档的状态</span></span>
<span class="line"><span style="color:#E1E4E8;">document.hidden  </span><span style="color:#6A737D;">//返回一个布尔值，表示当前页面是否可见</span></span>
<span class="line"><span style="color:#E1E4E8;">document.visibilityState  </span><span style="color:#6A737D;">//返回文档的可见状态</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//其他属性</span></span>
<span class="line"><span style="color:#E1E4E8;">document.cookie   </span><span style="color:#6A737D;">//用来操作Cookie</span></span>
<span class="line"><span style="color:#E1E4E8;">document.designMode  </span><span style="color:#6A737D;">//控制当前文档是否可编辑，可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">document.currentScript  </span><span style="color:#6A737D;">//返回当前脚本所在的那个 DOM 节点，即&lt;script&gt;元素的 DOM 节点</span></span>
<span class="line"><span style="color:#E1E4E8;">document.implementation  </span><span style="color:#6A737D;">//返回一个DOMImplementation对象</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//元素特性相关属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.id  </span><span style="color:#6A737D;">//返回指定元素的id属性，可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.tagName  </span><span style="color:#6A737D;">//返回指定元素的大写标签名</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.dir </span><span style="color:#6A737D;">//用于读写当前元素的文字方向</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.accessKey  </span><span style="color:#6A737D;">//用于读写分配给当前元素的快捷键</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.draggble </span><span style="color:#6A737D;">//返回一个布尔值，表示当前元素是否可拖动</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.lang </span><span style="color:#6A737D;">//返回当前元素的语言设置。该属性可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">Elemnt.tabIndex </span><span style="color:#6A737D;">//返回一个整数，表示当前元素在 Tab 键遍历时的顺序。该属性可读写。</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.title  </span><span style="color:#6A737D;">//用来读写当前元素的 HTML 属性title</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.attributes  </span><span style="color:#6A737D;">//返回当前元素节点的所有属性节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.className  </span><span style="color:#6A737D;">//返回当前元素的class属性，可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.classList  </span><span style="color:#6A737D;">//返回当前元素节点的所有class集合</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.innerHTML   </span><span style="color:#6A737D;">//返回该元素包含的HTML代码，可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.outerHTML  </span><span style="color:#6A737D;">//返回指定元素节点的所有HTML代码，包括它自身和包含的的所有子元素，可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.dataset   </span><span style="color:#6A737D;">//返回元素节点中所有的data-*属性</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//元素状态的相关属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.hidden </span><span style="color:#6A737D;">//返回一个布尔值，表示当前元素的hidden属性，用来控制当前元素是否可见。该属性可读写</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.contentEditable </span><span style="color:#6A737D;">//返回一个字符串，表示是否设置了contenteditable属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.iscontentEditable </span><span style="color:#6A737D;">//返回一布尔值，表示是否设置了contenteditable属性，只读</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//尺寸属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.clientHeight   </span><span style="color:#6A737D;">//返回元素节点可见部分的高度</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.clientWidth   </span><span style="color:#6A737D;">//返回元素节点可见部分的宽度</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.clientLeft   </span><span style="color:#6A737D;">//返回元素节点左边框的宽度</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.clientTop   </span><span style="color:#6A737D;">//返回元素节点顶部边框的宽度</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.scrollHeight  </span><span style="color:#6A737D;">//返回元素节点的总高度</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.scrollWidth  </span><span style="color:#6A737D;">//返回元素节点的总宽度</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.scrollLeft   </span><span style="color:#6A737D;">//返回元素节点的水平滚动条向右滚动的像素数值,通过设置这个属性可以改变元素的滚动位置</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.scrollTop   </span><span style="color:#6A737D;">//返回元素节点的垂直滚动向下滚动的像素数值</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.offsetParent  </span><span style="color:#6A737D;">//返回最靠近当前元素的、并且 CSS 的position属性不等于static的上层元素</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.offsetHeight   </span><span style="color:#6A737D;">//返回元素的垂直高度(包含border,padding)</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.offsetWidth    </span><span style="color:#6A737D;">//返回元素的水平宽度(包含border,padding)</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.offsetLeft    </span><span style="color:#6A737D;">//返回当前元素左上角相对于Element.offsetParent节点的垂直偏移</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.offsetTop   </span><span style="color:#6A737D;">//返回水平位移</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.style  </span><span style="color:#6A737D;">//返回元素节点的行内样式</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//节点相关属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.children   </span><span style="color:#6A737D;">//包括当前元素节点的所有子元素</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.childElementCount   </span><span style="color:#6A737D;">//返回当前元素节点包含的子HTML元素节点的个数</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.firstElementChild  </span><span style="color:#6A737D;">//返回当前节点的第一个Element子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.lastElementChild   </span><span style="color:#6A737D;">//返回当前节点的最后一个Element子节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.nextElementSibling  </span><span style="color:#6A737D;">//返回当前元素节点的下一个兄弟HTML元素节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.previousElementSibling  </span><span style="color:#6A737D;">//返回当前元素节点的前一个兄弟HTML节点</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//属性方法</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">getAttribute</span><span style="color:#E1E4E8;">()：读取指定属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">getAttributeNames</span><span style="color:#E1E4E8;">()：返回当前元素的所有属性名</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">setAttribute</span><span style="color:#E1E4E8;">()：设置指定属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">hasAttribute</span><span style="color:#E1E4E8;">()：返回一个布尔值，表示当前元素节点是否有指定的属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">hasAttributes</span><span style="color:#E1E4E8;">()：当前元素是否有属性</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">removeAttribute</span><span style="color:#E1E4E8;">()：移除指定属性</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//查找方法</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">querySelector</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//接受 CSS 选择器作为参数，返回父元素的第一个匹配的子元素</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">querySelectorAll</span><span style="color:#E1E4E8;">() </span><span style="color:#6A737D;">//接受 CSS 选择器作为参数，返回一个NodeList实例，包含所有匹配的子元素</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">getElementsByTagName</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//返回一个HTMLCollection实例，成员是当前节点的所有匹配指定标签名的子元素节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">getElementsByClassName</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//返回一个HTMLCollection实例，成员是当前元素节点的所有具有指定 class 的子元素节点</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">closest</span><span style="color:#E1E4E8;">() </span><span style="color:#6A737D;">//接受一个 CSS 选择器作为参数，返回匹配该选择器的、最接近当前节点的一个祖先节点（包括当前节点本身）</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//事件方法</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">addEventListener</span><span style="color:#E1E4E8;">()：添加事件的回调函数</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">removeEventListener</span><span style="color:#E1E4E8;">()：移除事件监听函数</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">dispatchEvent</span><span style="color:#E1E4E8;">()：触发事件</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//其他</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">matches</span><span style="color:#E1E4E8;">() </span><span style="color:#6A737D;">//返回一个布尔值，表示当前元素是否匹配给定的 CSS 选择器</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">scrollIntoView</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//滚动当前元素，进入浏览器的可见区域</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">getBoundingClientRect</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//返回一个对象，包含top,left,right,bottom,width,height</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">getClientRects</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//返回当前元素在页面上形参的所有矩形</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//解析HTML字符串，然后将生成的节点插入DOM树的指定位置</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">insertAdjacentHTML</span><span style="color:#E1E4E8;">(where, htmlString);</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">insertAdjacentHTML</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;beforeBegin&#39;</span><span style="color:#E1E4E8;">, htmlString); </span><span style="color:#6A737D;">// 在该元素前插入</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">insertAdjacentHTML</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;afterBegin&#39;</span><span style="color:#E1E4E8;">, htmlString); </span><span style="color:#6A737D;">// 在该元素第一个子元素前插入</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">insertAdjacentHTML</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;beforeEnd&#39;</span><span style="color:#E1E4E8;">, htmlString); </span><span style="color:#6A737D;">// 在该元素最后一个子元素后面插入</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">insertAdjacentHTML</span><span style="color:#E1E4E8;">(</span><span style="color:#9ECBFF;">&#39;afterEnd&#39;</span><span style="color:#E1E4E8;">, htmlString); </span><span style="color:#6A737D;">// 在该元素后插入</span></span>
<span class="line"></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">remove</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//用于将当前元素节点从DOM中移除</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">focus</span><span style="color:#E1E4E8;">()   </span><span style="color:#6A737D;">//用于将当前页面的焦点，转移到指定元素上</span></span>
<span class="line"><span style="color:#E1E4E8;">Element.</span><span style="color:#B392F0;">blur</span><span style="color:#E1E4E8;">()  </span><span style="color:#6A737D;">//用于将焦点从当前元素移除</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#D73A49;">var</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">let</span><span style="color:#24292E;"> const</span></span>
<span class="line"><span style="color:#24292E;">声明变量</span></span>
<span class="line"><span style="color:#D73A49;">if</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">else</span></span>
<span class="line"><span style="color:#24292E;">条件语句</span></span>
<span class="line"><span style="color:#D73A49;">switch</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">case</span></span>
<span class="line"><span style="color:#24292E;">条件语句</span></span>
<span class="line"><span style="color:#24292E;">alert confirm prompt</span></span>
<span class="line"><span style="color:#24292E;">弹框</span></span>
<span class="line"><span style="color:#D73A49;">function</span></span>
<span class="line"><span style="color:#6F42C1;">函数</span></span>
<span class="line"><span style="color:#24292E;">for</span></span>
<span class="line"><span style="color:#24292E;">循环</span></span>
<span class="line"><span style="color:#D73A49;">while</span></span>
<span class="line"><span style="color:#24292E;">循环</span></span>
<span class="line"><span style="color:#D73A49;">break</span></span>
<span class="line"><span style="color:#24292E;">终止循环</span></span>
<span class="line"><span style="color:#D73A49;">continue</span></span>
<span class="line"><span style="color:#24292E;">跳过循环</span></span>
<span class="line"><span style="color:#24292E;">onLoad onUnload onFocus onBlur onChange onSubmit onClick onMouseOver onMouseOut</span></span>
<span class="line"><span style="color:#24292E;">事件太多了随便列举几个</span></span>
<span class="line"><span style="color:#D73A49;">try</span><span style="color:#24292E;"> </span><span style="color:#D73A49;">catch</span></span>
<span class="line"><span style="color:#24292E;">异常捕获</span></span>
<span class="line"><span style="color:#D73A49;">throw</span></span>
<span class="line"><span style="color:#24292E;">抛异常</span></span>
<span class="line"><span style="color:#24292E;">String Number Boolean Null Object Array Symbol BigInt </span><span style="color:#005CC5;">undefined</span></span>
<span class="line"><span style="color:#24292E;">基本类型</span></span>
<span class="line"><span style="color:#005CC5;">Promise</span><span style="color:#24292E;"> then </span><span style="color:#D73A49;">catch</span><span style="color:#24292E;"> reject resolve all any race allSettled</span></span>
<span class="line"><span style="color:#005CC5;">DOM</span></span>
<span class="line"><span style="color:#24292E;">window location screen history Navigator</span></span>
<span class="line"><span style="color:#005CC5;">BOM</span></span>
<span class="line"><span style="color:#24292E;">Node.nodeName   </span><span style="color:#6A737D;">//返回节点名称</span></span>
<span class="line"><span style="color:#24292E;">Node.nodeType   </span><span style="color:#6A737D;">//返回节点类型的常数值</span></span>
<span class="line"><span style="color:#24292E;">Node.nodeValue  </span><span style="color:#6A737D;">//返回Text或Comment节点的文本值</span></span>
<span class="line"><span style="color:#24292E;">Node.textContent  </span><span style="color:#6A737D;">//返回当前节点和它的所有后代节点的文本内容，可读写</span></span>
<span class="line"><span style="color:#24292E;">Node.baseURI    </span><span style="color:#6A737D;">//返回当前网页的绝对路径</span></span>
<span class="line"><span style="color:#24292E;">Node.ownerDocument  </span><span style="color:#6A737D;">//返回当前节点所在的顶层文档对象，即document</span></span>
<span class="line"><span style="color:#24292E;">Node.nextSibling  </span><span style="color:#6A737D;">//返回紧跟在当前节点后面的第一个兄弟节点</span></span>
<span class="line"><span style="color:#24292E;">Node.previousSibling  </span><span style="color:#6A737D;">//返回当前节点前面的、距离最近的一个兄弟节点</span></span>
<span class="line"><span style="color:#24292E;">Node.parentNode   </span><span style="color:#6A737D;">//返回当前节点的父节点</span></span>
<span class="line"><span style="color:#24292E;">Node.parentElement  </span><span style="color:#6A737D;">//返回当前节点的父Element节点</span></span>
<span class="line"><span style="color:#24292E;">Node.childNodes   </span><span style="color:#6A737D;">//返回当前节点的所有子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.firstChild  </span><span style="color:#6A737D;">//返回当前节点的第一个子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.lastChild   </span><span style="color:#6A737D;">//返回当前节点的最后一个子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.isconnecoted   </span><span style="color:#6A737D;">//返回一个布尔值，表示当前节点是否在文档之中</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">appendChild</span><span style="color:#24292E;">(node)   </span><span style="color:#6A737D;">//向节点添加最后一个子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">hasChildNodes</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//返回布尔值，表示当前节点是否有子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">cloneNode</span><span style="color:#24292E;">(</span><span style="color:#005CC5;">true</span><span style="color:#24292E;">);  </span><span style="color:#6A737D;">// 默认为false(克隆节点), true(克隆节点及其属性，以及后代)</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">insertBefore</span><span style="color:#24292E;">(newNode,oldNode)  </span><span style="color:#6A737D;">// 在指定子节点之前插入新的子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">removeChild</span><span style="color:#24292E;">(node)   </span><span style="color:#6A737D;">//删除节点，在要删除节点的父节点上操作</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">replaceChild</span><span style="color:#24292E;">(newChild,oldChild)  </span><span style="color:#6A737D;">//替换节点</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">contains</span><span style="color:#24292E;">(node)  </span><span style="color:#6A737D;">//返回一个布尔值，表示参数节点是否为当前节点的后代节点。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">compareDocumentPosition</span><span style="color:#24292E;">(node)   </span><span style="color:#6A737D;">//返回一个7个比特位的二进制值，表示参数节点和当前节点的关系</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">isEqualNode</span><span style="color:#24292E;">(node)  </span><span style="color:#6A737D;">//返回布尔值，用于检查两个节点是否相等。所谓相等的节点，指的是两个节点的类型相同、属性相同、子节点相同。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">isSameNode</span><span style="color:#24292E;">(node)  </span><span style="color:#6A737D;">//返回一个布尔值，表示两个节点是否为同一个节点。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">normalize</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//用于清理当前节点内部的所有Text节点。它会去除空的文本节点，并且将毗邻的文本节点合并成一个。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">getRootNode</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//返回当前节点所在文档的根节点document，与ownerDocument属性的作用相同。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#005CC5;">length</span><span style="color:#24292E;">   </span><span style="color:#6A737D;">//返回 NodeList 实例包含的节点数量</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">forEach</span><span style="color:#24292E;">(fn，</span><span style="color:#005CC5;">this</span><span style="color:#24292E;">)   </span><span style="color:#6A737D;">//用于遍历 NodeList 的所有成员</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">item</span><span style="color:#24292E;">(index) </span><span style="color:#6A737D;">// 接受一个整数值作为参数，表示成员的位置，返回该位置上的成员</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">keys</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//返回键名的遍历器</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">values</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//返回键值的遍历器</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">entries</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//返回的遍历器同时包含键名和键值的信息</span></span>
<span class="line"><span style="color:#6A737D;">//parentNode接口</span></span>
<span class="line"><span style="color:#24292E;">Node.children  </span><span style="color:#6A737D;">//返回指定节点的所有Element子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.firstElementChild  </span><span style="color:#6A737D;">//返回当前节点的第一个Element子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.lastElementChild   </span><span style="color:#6A737D;">//返回当前节点的最后一个Element子节点</span></span>
<span class="line"><span style="color:#24292E;">Node.childElementCount  </span><span style="color:#6A737D;">//返回当前节点所有Element子节点的数目</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">append</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//为当前节点追加一个或多个子节点，位置是最后一个元素子节点的后面。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">prepend</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//为当前节点追加一个或多个子节点，位置是第一个元素子节点的前面。</span></span>
<span class="line"><span style="color:#6A737D;">//ChildNode接口</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">remove</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//用于从父节点移除当前节点</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">before</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//用于在当前节点的前面，插入一个或多个同级节点。两者拥有相同的父节点。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">after</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//用于在当前节点的后面，插入一个或多个同级节点，两者拥有相同的父节点。</span></span>
<span class="line"><span style="color:#24292E;">Node.</span><span style="color:#6F42C1;">replaceWith</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//使用参数节点，替换当前节点</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">document</span></span>
<span class="line"><span style="color:#6A737D;">//快捷方式属性</span></span>
<span class="line"><span style="color:#24292E;">document.defaultView </span><span style="color:#6A737D;">//返回document对象所属的window对象</span></span>
<span class="line"><span style="color:#24292E;">document.doctype   </span><span style="color:#6A737D;">//返回文档类型节点</span></span>
<span class="line"><span style="color:#24292E;">document.documentElement  </span><span style="color:#6A737D;">//返回当前文档的根节点</span></span>
<span class="line"><span style="color:#24292E;">document.body   </span><span style="color:#6A737D;">//返回当前文档的&lt;body&gt;节点</span></span>
<span class="line"><span style="color:#24292E;">document.head   </span><span style="color:#6A737D;">//返回当前文档的&lt;head&gt;节点</span></span>
<span class="line"><span style="color:#24292E;">document.activeElement  </span><span style="color:#6A737D;">//返回当前文档中获得焦点的那个元素</span></span>
<span class="line"><span style="color:#24292E;">document.fullscreenElement  </span><span style="color:#6A737D;">//返回当前以全屏状态展示的 DOM 元素</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//节点集合属性</span></span>
<span class="line"><span style="color:#24292E;">document.links  </span><span style="color:#6A737D;">//返回当前文档的所有a元素</span></span>
<span class="line"><span style="color:#24292E;">document.forms  </span><span style="color:#6A737D;">//返回页面中所有表单元素</span></span>
<span class="line"><span style="color:#24292E;">document.images  </span><span style="color:#6A737D;">//返回页面中所有图片元素</span></span>
<span class="line"><span style="color:#24292E;">document.embeds  </span><span style="color:#6A737D;">//返回网页中所有嵌入对象</span></span>
<span class="line"><span style="color:#24292E;">document.scripts  </span><span style="color:#6A737D;">//返回当前文档的所有脚本</span></span>
<span class="line"><span style="color:#24292E;">document.styleSheets  </span><span style="color:#6A737D;">//返回当前网页的所有样式表</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//文档静态信息属性</span></span>
<span class="line"><span style="color:#24292E;">document.documentURI  </span><span style="color:#6A737D;">//表示当前文档的网址</span></span>
<span class="line"><span style="color:#24292E;">document.</span><span style="color:#005CC5;">URL</span><span style="color:#24292E;">  </span><span style="color:#6A737D;">//返回当前文档的网址</span></span>
<span class="line"><span style="color:#24292E;">document.domain  </span><span style="color:#6A737D;">//返回当前文档的域名</span></span>
<span class="line"><span style="color:#24292E;">document.lastModified  </span><span style="color:#6A737D;">//返回当前文档最后修改的时间</span></span>
<span class="line"><span style="color:#24292E;">document.location  </span><span style="color:#6A737D;">//返回location对象，提供当前文档的URL信息</span></span>
<span class="line"><span style="color:#24292E;">document.referrer  </span><span style="color:#6A737D;">//返回当前文档的访问来源</span></span>
<span class="line"><span style="color:#24292E;">document.title    </span><span style="color:#6A737D;">//返回当前文档的标题</span></span>
<span class="line"><span style="color:#24292E;">document.compatMode  </span><span style="color:#6A737D;">//返回浏览器处理文档的模式</span></span>
<span class="line"><span style="color:#24292E;">document.characterSet  </span><span style="color:#6A737D;">//返回渲染当前文档的字符集，比如UTF-8、ISO-8859-1</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//文档状态属性</span></span>
<span class="line"><span style="color:#24292E;">document.readyState  </span><span style="color:#6A737D;">//返回当前文档的状态</span></span>
<span class="line"><span style="color:#24292E;">document.hidden  </span><span style="color:#6A737D;">//返回一个布尔值，表示当前页面是否可见</span></span>
<span class="line"><span style="color:#24292E;">document.visibilityState  </span><span style="color:#6A737D;">//返回文档的可见状态</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//其他属性</span></span>
<span class="line"><span style="color:#24292E;">document.cookie   </span><span style="color:#6A737D;">//用来操作Cookie</span></span>
<span class="line"><span style="color:#24292E;">document.designMode  </span><span style="color:#6A737D;">//控制当前文档是否可编辑，可读写</span></span>
<span class="line"><span style="color:#24292E;">document.currentScript  </span><span style="color:#6A737D;">//返回当前脚本所在的那个 DOM 节点，即&lt;script&gt;元素的 DOM 节点</span></span>
<span class="line"><span style="color:#24292E;">document.implementation  </span><span style="color:#6A737D;">//返回一个DOMImplementation对象</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//元素特性相关属性</span></span>
<span class="line"><span style="color:#24292E;">Element.id  </span><span style="color:#6A737D;">//返回指定元素的id属性，可读写</span></span>
<span class="line"><span style="color:#24292E;">Element.tagName  </span><span style="color:#6A737D;">//返回指定元素的大写标签名</span></span>
<span class="line"><span style="color:#24292E;">Element.dir </span><span style="color:#6A737D;">//用于读写当前元素的文字方向</span></span>
<span class="line"><span style="color:#24292E;">Element.accessKey  </span><span style="color:#6A737D;">//用于读写分配给当前元素的快捷键</span></span>
<span class="line"><span style="color:#24292E;">Element.draggble </span><span style="color:#6A737D;">//返回一个布尔值，表示当前元素是否可拖动</span></span>
<span class="line"><span style="color:#24292E;">Element.lang </span><span style="color:#6A737D;">//返回当前元素的语言设置。该属性可读写</span></span>
<span class="line"><span style="color:#24292E;">Elemnt.tabIndex </span><span style="color:#6A737D;">//返回一个整数，表示当前元素在 Tab 键遍历时的顺序。该属性可读写。</span></span>
<span class="line"><span style="color:#24292E;">Element.title  </span><span style="color:#6A737D;">//用来读写当前元素的 HTML 属性title</span></span>
<span class="line"><span style="color:#24292E;">Element.attributes  </span><span style="color:#6A737D;">//返回当前元素节点的所有属性节点</span></span>
<span class="line"><span style="color:#24292E;">Element.className  </span><span style="color:#6A737D;">//返回当前元素的class属性，可读写</span></span>
<span class="line"><span style="color:#24292E;">Element.classList  </span><span style="color:#6A737D;">//返回当前元素节点的所有class集合</span></span>
<span class="line"><span style="color:#24292E;">Element.innerHTML   </span><span style="color:#6A737D;">//返回该元素包含的HTML代码，可读写</span></span>
<span class="line"><span style="color:#24292E;">Element.outerHTML  </span><span style="color:#6A737D;">//返回指定元素节点的所有HTML代码，包括它自身和包含的的所有子元素，可读写</span></span>
<span class="line"><span style="color:#24292E;">Element.dataset   </span><span style="color:#6A737D;">//返回元素节点中所有的data-*属性</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//元素状态的相关属性</span></span>
<span class="line"><span style="color:#24292E;">Element.hidden </span><span style="color:#6A737D;">//返回一个布尔值，表示当前元素的hidden属性，用来控制当前元素是否可见。该属性可读写</span></span>
<span class="line"><span style="color:#24292E;">Element.contentEditable </span><span style="color:#6A737D;">//返回一个字符串，表示是否设置了contenteditable属性</span></span>
<span class="line"><span style="color:#24292E;">Element.iscontentEditable </span><span style="color:#6A737D;">//返回一布尔值，表示是否设置了contenteditable属性，只读</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//尺寸属性</span></span>
<span class="line"><span style="color:#24292E;">Element.clientHeight   </span><span style="color:#6A737D;">//返回元素节点可见部分的高度</span></span>
<span class="line"><span style="color:#24292E;">Element.clientWidth   </span><span style="color:#6A737D;">//返回元素节点可见部分的宽度</span></span>
<span class="line"><span style="color:#24292E;">Element.clientLeft   </span><span style="color:#6A737D;">//返回元素节点左边框的宽度</span></span>
<span class="line"><span style="color:#24292E;">Element.clientTop   </span><span style="color:#6A737D;">//返回元素节点顶部边框的宽度</span></span>
<span class="line"><span style="color:#24292E;">Element.scrollHeight  </span><span style="color:#6A737D;">//返回元素节点的总高度</span></span>
<span class="line"><span style="color:#24292E;">Element.scrollWidth  </span><span style="color:#6A737D;">//返回元素节点的总宽度</span></span>
<span class="line"><span style="color:#24292E;">Element.scrollLeft   </span><span style="color:#6A737D;">//返回元素节点的水平滚动条向右滚动的像素数值,通过设置这个属性可以改变元素的滚动位置</span></span>
<span class="line"><span style="color:#24292E;">Element.scrollTop   </span><span style="color:#6A737D;">//返回元素节点的垂直滚动向下滚动的像素数值</span></span>
<span class="line"><span style="color:#24292E;">Element.offsetParent  </span><span style="color:#6A737D;">//返回最靠近当前元素的、并且 CSS 的position属性不等于static的上层元素</span></span>
<span class="line"><span style="color:#24292E;">Element.offsetHeight   </span><span style="color:#6A737D;">//返回元素的垂直高度(包含border,padding)</span></span>
<span class="line"><span style="color:#24292E;">Element.offsetWidth    </span><span style="color:#6A737D;">//返回元素的水平宽度(包含border,padding)</span></span>
<span class="line"><span style="color:#24292E;">Element.offsetLeft    </span><span style="color:#6A737D;">//返回当前元素左上角相对于Element.offsetParent节点的垂直偏移</span></span>
<span class="line"><span style="color:#24292E;">Element.offsetTop   </span><span style="color:#6A737D;">//返回水平位移</span></span>
<span class="line"><span style="color:#24292E;">Element.style  </span><span style="color:#6A737D;">//返回元素节点的行内样式</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//节点相关属性</span></span>
<span class="line"><span style="color:#24292E;">Element.children   </span><span style="color:#6A737D;">//包括当前元素节点的所有子元素</span></span>
<span class="line"><span style="color:#24292E;">Element.childElementCount   </span><span style="color:#6A737D;">//返回当前元素节点包含的子HTML元素节点的个数</span></span>
<span class="line"><span style="color:#24292E;">Element.firstElementChild  </span><span style="color:#6A737D;">//返回当前节点的第一个Element子节点</span></span>
<span class="line"><span style="color:#24292E;">Element.lastElementChild   </span><span style="color:#6A737D;">//返回当前节点的最后一个Element子节点</span></span>
<span class="line"><span style="color:#24292E;">Element.nextElementSibling  </span><span style="color:#6A737D;">//返回当前元素节点的下一个兄弟HTML元素节点</span></span>
<span class="line"><span style="color:#24292E;">Element.previousElementSibling  </span><span style="color:#6A737D;">//返回当前元素节点的前一个兄弟HTML节点</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//属性方法</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">getAttribute</span><span style="color:#24292E;">()：读取指定属性</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">getAttributeNames</span><span style="color:#24292E;">()：返回当前元素的所有属性名</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">setAttribute</span><span style="color:#24292E;">()：设置指定属性</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">hasAttribute</span><span style="color:#24292E;">()：返回一个布尔值，表示当前元素节点是否有指定的属性</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">hasAttributes</span><span style="color:#24292E;">()：当前元素是否有属性</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">removeAttribute</span><span style="color:#24292E;">()：移除指定属性</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//查找方法</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">querySelector</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//接受 CSS 选择器作为参数，返回父元素的第一个匹配的子元素</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">querySelectorAll</span><span style="color:#24292E;">() </span><span style="color:#6A737D;">//接受 CSS 选择器作为参数，返回一个NodeList实例，包含所有匹配的子元素</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">getElementsByTagName</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//返回一个HTMLCollection实例，成员是当前节点的所有匹配指定标签名的子元素节点</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">getElementsByClassName</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//返回一个HTMLCollection实例，成员是当前元素节点的所有具有指定 class 的子元素节点</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">closest</span><span style="color:#24292E;">() </span><span style="color:#6A737D;">//接受一个 CSS 选择器作为参数，返回匹配该选择器的、最接近当前节点的一个祖先节点（包括当前节点本身）</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//事件方法</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">addEventListener</span><span style="color:#24292E;">()：添加事件的回调函数</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">removeEventListener</span><span style="color:#24292E;">()：移除事件监听函数</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">dispatchEvent</span><span style="color:#24292E;">()：触发事件</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//其他</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">matches</span><span style="color:#24292E;">() </span><span style="color:#6A737D;">//返回一个布尔值，表示当前元素是否匹配给定的 CSS 选择器</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">scrollIntoView</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//滚动当前元素，进入浏览器的可见区域</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">getBoundingClientRect</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//返回一个对象，包含top,left,right,bottom,width,height</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">getClientRects</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//返回当前元素在页面上形参的所有矩形</span></span>
<span class="line"></span>
<span class="line"><span style="color:#6A737D;">//解析HTML字符串，然后将生成的节点插入DOM树的指定位置</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">insertAdjacentHTML</span><span style="color:#24292E;">(where, htmlString);</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">insertAdjacentHTML</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;beforeBegin&#39;</span><span style="color:#24292E;">, htmlString); </span><span style="color:#6A737D;">// 在该元素前插入</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">insertAdjacentHTML</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;afterBegin&#39;</span><span style="color:#24292E;">, htmlString); </span><span style="color:#6A737D;">// 在该元素第一个子元素前插入</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">insertAdjacentHTML</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;beforeEnd&#39;</span><span style="color:#24292E;">, htmlString); </span><span style="color:#6A737D;">// 在该元素最后一个子元素后面插入</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">insertAdjacentHTML</span><span style="color:#24292E;">(</span><span style="color:#032F62;">&#39;afterEnd&#39;</span><span style="color:#24292E;">, htmlString); </span><span style="color:#6A737D;">// 在该元素后插入</span></span>
<span class="line"></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">remove</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//用于将当前元素节点从DOM中移除</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">focus</span><span style="color:#24292E;">()   </span><span style="color:#6A737D;">//用于将当前页面的焦点，转移到指定元素上</span></span>
<span class="line"><span style="color:#24292E;">Element.</span><span style="color:#6F42C1;">blur</span><span style="color:#24292E;">()  </span><span style="color:#6A737D;">//用于将焦点从当前元素移除</span></span></code></pre><div class="line-numbers-wrapper" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br><span class="line-number">104</span><br><span class="line-number">105</span><br><span class="line-number">106</span><br><span class="line-number">107</span><br><span class="line-number">108</span><br><span class="line-number">109</span><br><span class="line-number">110</span><br><span class="line-number">111</span><br><span class="line-number">112</span><br><span class="line-number">113</span><br><span class="line-number">114</span><br><span class="line-number">115</span><br><span class="line-number">116</span><br><span class="line-number">117</span><br><span class="line-number">118</span><br><span class="line-number">119</span><br><span class="line-number">120</span><br><span class="line-number">121</span><br><span class="line-number">122</span><br><span class="line-number">123</span><br><span class="line-number">124</span><br><span class="line-number">125</span><br><span class="line-number">126</span><br><span class="line-number">127</span><br><span class="line-number">128</span><br><span class="line-number">129</span><br><span class="line-number">130</span><br><span class="line-number">131</span><br><span class="line-number">132</span><br><span class="line-number">133</span><br><span class="line-number">134</span><br><span class="line-number">135</span><br><span class="line-number">136</span><br><span class="line-number">137</span><br><span class="line-number">138</span><br><span class="line-number">139</span><br><span class="line-number">140</span><br><span class="line-number">141</span><br><span class="line-number">142</span><br><span class="line-number">143</span><br><span class="line-number">144</span><br><span class="line-number">145</span><br><span class="line-number">146</span><br><span class="line-number">147</span><br><span class="line-number">148</span><br><span class="line-number">149</span><br><span class="line-number">150</span><br><span class="line-number">151</span><br><span class="line-number">152</span><br><span class="line-number">153</span><br><span class="line-number">154</span><br><span class="line-number">155</span><br><span class="line-number">156</span><br><span class="line-number">157</span><br><span class="line-number">158</span><br><span class="line-number">159</span><br><span class="line-number">160</span><br><span class="line-number">161</span><br><span class="line-number">162</span><br><span class="line-number">163</span><br><span class="line-number">164</span><br><span class="line-number">165</span><br><span class="line-number">166</span><br><span class="line-number">167</span><br><span class="line-number">168</span><br><span class="line-number">169</span><br><span class="line-number">170</span><br><span class="line-number">171</span><br><span class="line-number">172</span><br><span class="line-number">173</span><br><span class="line-number">174</span><br><span class="line-number">175</span><br><span class="line-number">176</span><br><span class="line-number">177</span><br><span class="line-number">178</span><br><span class="line-number">179</span><br><span class="line-number">180</span><br><span class="line-number">181</span><br><span class="line-number">182</span><br><span class="line-number">183</span><br><span class="line-number">184</span><br><span class="line-number">185</span><br><span class="line-number">186</span><br><span class="line-number">187</span><br><span class="line-number">188</span><br><span class="line-number">189</span><br><span class="line-number">190</span><br><span class="line-number">191</span><br><span class="line-number">192</span><br><span class="line-number">193</span><br><span class="line-number">194</span><br><span class="line-number">195</span><br><span class="line-number">196</span><br></div></div>`,2),o=[e];function c(t,r,E,i,y,m){return n(),a("div",null,o)}const u=s(p,[["render",c]]);export{d as __pageData,u as default};
