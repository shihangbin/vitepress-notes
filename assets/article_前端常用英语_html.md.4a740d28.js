import{_ as s,o as n,c as a,Q as l}from"./chunks/framework.f25890ca.js";const u=JSON.parse('{"title":"HTML (常用单词)","description":"","frontmatter":{},"headers":[],"relativePath":"article/前端常用英语/html.md","filePath":"article/前端常用英语/html.md","lastUpdated":1690446826000}'),p={name:"article/前端常用英语/html.md"},o=l(`<h1 id="html-常用单词" tabindex="-1">HTML (常用单词) <a class="header-anchor" href="#html-常用单词" aria-label="Permalink to &quot;HTML (常用单词)&quot;">​</a></h1><div class="language-html vp-adaptive-theme line-numbers-mode"><button title="Copy Code" class="copy"></button><span class="lang">html</span><pre class="shiki github-dark vp-code-dark"><code><span class="line"><span style="color:#FDAEB7;font-style:italic;">&lt;</span><span style="color:#E1E4E8;">!DOCTYPE&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">声明必须位于 HTML5 文档中的第一行，也就是位于 &lt;</span><span style="color:#85E89D;">html</span><span style="color:#E1E4E8;">&gt; 标签之前。该标签告知浏览器文档所使用的 HTML 规范</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">html</span><span style="color:#E1E4E8;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示一个 HTML 文档的根（顶级元素），所以它也被称为*根元素*。所有其他元素必须是此元素的后代。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">head</span><span style="color:#E1E4E8;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">HTML head 元素 规定文档相关的配置信息（元数据），包括文档的标题，引用的文档样式和脚本等</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">meta</span><span style="color:#E1E4E8;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素可提供有关页面的元信息（meta-information），比如针对搜索引擎和更新频度的描述和关键词</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">body</span><span style="color:#E1E4E8;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示文档的内容。 属性提供了可以轻松访问文档的 body 元素的脚本</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">title</span><span style="color:#E1E4E8;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">定义文档的标题，显示在浏览器的标题栏或标签页上。它只应该包含文本，若是包含有标签，则它包含的任何标签都将被忽略</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">style</span><span style="color:#E1E4E8;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">定义 </span><span style="color:#85E89D;">HTML</span><span style="color:#E1E4E8;"> 文档的样式信息</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">link</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">大多数时候都用来连接样式</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">header</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">用于展示介绍性内容，通常包含一组介绍性的或是辅助导航的实用元素。它可能包含一些标题元素，但也可能包含其他元素，比如 Logo、搜索框、作者名称，等等</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">footer</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示最近一个章节内容或者根节点元素的页脚。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">article</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素表示文档、页面、应用或网站中的独立结构，其意在成为可独立分配的或可复用的结构，如在发布中，它可能是论坛帖子、杂志或新闻文章、博客、用户提交的评论、交互式组件，或者其他独立的内容项目</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">section</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示 </span><span style="color:#85E89D;">HTML</span><span style="color:#E1E4E8;"> 文档中一个通用独立章节，它没有更具体的语义元素来表示。一般来说会包含一个标题。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">p</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示文本的一个段落。该元素通常表现为一整块与相邻文本分离的文本，或以垂直的空白隔离或以首行缩进（块级元素）</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">div</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">是一个通用型的流内容容器，在不使用CSS其对内容或布局没有任何影响（块级元素）</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">span</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素是短语内容的通用行内容器，并没有任何特殊语义。(内联元素)</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">img</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素将一份图像嵌入文档</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">aside</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素表示一个和其余页面内容几乎无关的部分，被认为是独立于该内容的一部分并且可以被单独的拆分出来而不会使整体受影响</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">audio</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素用于在文档中嵌入音频内容</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">video</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素用于在文档中嵌入视频内容</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">source</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签为媒介元素（比如 &lt;</span><span style="color:#85E89D;">video</span><span style="color:#F97583;">&gt;</span><span style="color:#E1E4E8;"> 和 &lt;</span><span style="color:#85E89D;">audio</span><span style="color:#F97583;">&gt;</span><span style="color:#E1E4E8;">）定义媒介资源</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">canvas</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">元素可被用来通过 JavaScript（CanvasAPI 或 WebGL）绘制图形及图形动画</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">nav</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示页面的一部分，其目的是在当前文档或其他文档中提供导航链接。导航部分的常见示例是菜单，目录和索引</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">ul</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示一个内可含多个元素的无序列表或项目符号列表</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">ol</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表示有序列表，通常渲染为一个带编号的列表。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">li</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">用于表示列表里的条目。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">script</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">用于嵌入或引用可执行脚本。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">h1</span><span style="color:#E1E4E8;"> - </span><span style="color:#85E89D;">h6</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">代表网页中的标题数字越小越大</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">a</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签定义超链接，用于从一个页面链接到另一个页面。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">b</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签定义粗体的文本。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">br</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签插入简单的换行符。</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">hr</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签水平线</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">iframe</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">嵌套另一个网页</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">form</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">用于创建供用户输入的 </span><span style="color:#85E89D;">HTML</span><span style="color:#E1E4E8;"> 表单</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">input</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">表单输入元素</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">textarea</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">文本域</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">button</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">定义按钮</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">table</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">定义表格</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">tbody</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">定义一段表格主体</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">thead</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">定义表格的表头主体</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">tfoot</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">定义表格的页脚</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">tr</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签定义表格中的行</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">th</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签定义 </span><span style="color:#85E89D;">HTML</span><span style="color:#E1E4E8;"> 表格中的表头单元格</span></span>
<span class="line"><span style="color:#E1E4E8;">&lt;</span><span style="color:#85E89D;">td</span><span style="color:#F97583;">&gt;</span></span>
<span class="line"><span style="color:#E1E4E8;">标签定义 </span><span style="color:#85E89D;">HTML</span><span style="color:#E1E4E8;"> 表格中的标准单元格。</span></span></code></pre><pre class="shiki github-light vp-code-light"><code><span class="line"><span style="color:#B31D28;font-style:italic;">&lt;</span><span style="color:#24292E;">!DOCTYPE&gt;</span></span>
<span class="line"><span style="color:#24292E;">声明必须位于 HTML5 文档中的第一行，也就是位于 &lt;</span><span style="color:#22863A;">html</span><span style="color:#24292E;">&gt; 标签之前。该标签告知浏览器文档所使用的 HTML 规范</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">html</span><span style="color:#24292E;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示一个 HTML 文档的根（顶级元素），所以它也被称为*根元素*。所有其他元素必须是此元素的后代。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">head</span><span style="color:#24292E;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">HTML head 元素 规定文档相关的配置信息（元数据），包括文档的标题，引用的文档样式和脚本等</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">meta</span><span style="color:#24292E;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素可提供有关页面的元信息（meta-information），比如针对搜索引擎和更新频度的描述和关键词</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">body</span><span style="color:#24292E;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示文档的内容。 属性提供了可以轻松访问文档的 body 元素的脚本</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">title</span><span style="color:#24292E;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">定义文档的标题，显示在浏览器的标题栏或标签页上。它只应该包含文本，若是包含有标签，则它包含的任何标签都将被忽略</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">style</span><span style="color:#24292E;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">定义 </span><span style="color:#22863A;">HTML</span><span style="color:#24292E;"> 文档的样式信息</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">link</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">大多数时候都用来连接样式</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">header</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">用于展示介绍性内容，通常包含一组介绍性的或是辅助导航的实用元素。它可能包含一些标题元素，但也可能包含其他元素，比如 Logo、搜索框、作者名称，等等</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">footer</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示最近一个章节内容或者根节点元素的页脚。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">article</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素表示文档、页面、应用或网站中的独立结构，其意在成为可独立分配的或可复用的结构，如在发布中，它可能是论坛帖子、杂志或新闻文章、博客、用户提交的评论、交互式组件，或者其他独立的内容项目</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">section</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示 </span><span style="color:#22863A;">HTML</span><span style="color:#24292E;"> 文档中一个通用独立章节，它没有更具体的语义元素来表示。一般来说会包含一个标题。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">p</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示文本的一个段落。该元素通常表现为一整块与相邻文本分离的文本，或以垂直的空白隔离或以首行缩进（块级元素）</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">div</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">是一个通用型的流内容容器，在不使用CSS其对内容或布局没有任何影响（块级元素）</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">span</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素是短语内容的通用行内容器，并没有任何特殊语义。(内联元素)</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">img</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素将一份图像嵌入文档</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">aside</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素表示一个和其余页面内容几乎无关的部分，被认为是独立于该内容的一部分并且可以被单独的拆分出来而不会使整体受影响</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">audio</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素用于在文档中嵌入音频内容</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">video</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素用于在文档中嵌入视频内容</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">source</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签为媒介元素（比如 &lt;</span><span style="color:#22863A;">video</span><span style="color:#D73A49;">&gt;</span><span style="color:#24292E;"> 和 &lt;</span><span style="color:#22863A;">audio</span><span style="color:#D73A49;">&gt;</span><span style="color:#24292E;">）定义媒介资源</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">canvas</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">元素可被用来通过 JavaScript（CanvasAPI 或 WebGL）绘制图形及图形动画</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">nav</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示页面的一部分，其目的是在当前文档或其他文档中提供导航链接。导航部分的常见示例是菜单，目录和索引</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">ul</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示一个内可含多个元素的无序列表或项目符号列表</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">ol</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表示有序列表，通常渲染为一个带编号的列表。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">li</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">用于表示列表里的条目。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">script</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">用于嵌入或引用可执行脚本。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">h1</span><span style="color:#24292E;"> - </span><span style="color:#22863A;">h6</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">代表网页中的标题数字越小越大</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">a</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签定义超链接，用于从一个页面链接到另一个页面。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">b</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签定义粗体的文本。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">br</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签插入简单的换行符。</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">hr</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签水平线</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">iframe</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">嵌套另一个网页</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">form</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">用于创建供用户输入的 </span><span style="color:#22863A;">HTML</span><span style="color:#24292E;"> 表单</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">input</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">表单输入元素</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">textarea</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">文本域</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">button</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">定义按钮</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">table</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">定义表格</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">tbody</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">定义一段表格主体</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">thead</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">定义表格的表头主体</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">tfoot</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">定义表格的页脚</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">tr</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签定义表格中的行</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">th</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签定义 </span><span style="color:#22863A;">HTML</span><span style="color:#24292E;"> 表格中的表头单元格</span></span>
<span class="line"><span style="color:#24292E;">&lt;</span><span style="color:#22863A;">td</span><span style="color:#D73A49;">&gt;</span></span>
<span class="line"><span style="color:#24292E;">标签定义 </span><span style="color:#22863A;">HTML</span><span style="color:#24292E;"> 表格中的标准单元格。</span></span></code></pre><div class="line-numbers-wrapper" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br></div></div>`,2),e=[o];function c(t,r,E,y,i,b){return n(),a("div",null,e)}const g=s(p,[["render",c]]);export{u as __pageData,g as default};
